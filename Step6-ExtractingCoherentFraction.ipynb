{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ed2d4fd1",
   "metadata": {},
   "source": [
    "#  Coherent J/$\\psi$ production in ultraperipheral PbPb collisions at $\\sqrt{s_{_{\\text{NN}}}}=5.02$ TeV"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "75b9a8ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "import ROOT\n",
    "import sys\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "sys.path.append('snippets')\n",
    "from helpers import *\n",
    "from fit_plot import *\n",
    "ROOT.gStyle.SetOptStat(0)\n",
    "ROOT.gStyle.SetOptTitle(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9715a21",
   "metadata": {},
   "source": [
    "## Extracting coherent J/$\\psi$ fraction\n",
    "\n",
    "This notebook is part of the Heavy Ion Exercise for the CERN CMSDAS School 2023.\n",
    "\n",
    "We assume that you have skimmed the data and simulation, made a first inspection and you are now ready to dive into the core of the analysis.\n",
    "Let's continue by fitting the J$/\\psi$ $p_\\text{T}$ spectra to extract the coherent J$/\\psi$ fraction."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "674b25b2",
   "metadata": {},
   "source": [
    "The simulations were produced using the [STARlight](https://starlight.hepforge.org/) generator and the CMS detector was simulated using [GEANT4](https://geant4.web.cern.ch/). STARlight is a MC generator that simulates two-photon and photon-Pomeron interactions between relativistic nuclei and protons. It is capable of producing several final states, such as coherent and incoherent vector mesons (e.g. J$/\\psi$ and $\\psi(\\text{2S})$) in photo-nuclear interactions.\n",
    "\n",
    "The following list of processes were simulated using STARlight:\n",
    "* **Coherent $\\text{J}/\\psi \\rightarrow \\mu^{+}\\mu^{-}$**, labelled as \"CohJpsiToMuMu\".\n",
    "* **Incoherent $\\text{J}/\\psi \\rightarrow \\mu^{+}\\mu^{-}$**, labelled as \"InCohJpsiToMuMu\".\n",
    "* **Coherent $\\psi(\\text{2S}) \\rightarrow \\text{J}/\\psi + \\text{X} \\rightarrow \\mu^{+}\\mu^{-} + \\text{X}$**, labelled as \"CohPsi2SFeeddownToMuMu\".\n",
    "* **QED background $\\gamma\\gamma \\rightarrow \\mu^{+}\\mu^{-}$**, labelled as \"LowMassGammaGammaToMuMu\".\n",
    "\n",
    "![diagram](img/feynmann_diagram.png)\n",
    "\n",
    "The process of **incoherent $\\text{J}/\\psi \\rightarrow \\mu^{+}\\mu^{-}$ with nucleon dissociation**, not included in the STARlight MC but present in data, is taken into account, to describe the large transverse momentum region, with a template based on the [H1 parameterisation](http://dx.doi.org/10.1140/epjc/s10052-013-2466-y) of this process, defined as:\n",
    "$$\n",
    "\\frac{\\text{d}N}{\\text{d}p_\\text{T}} \\approx p_\\text{T} \\times \\left(1 + \\left(\\frac{b_\\text{pd}}{n_\\text{pd}}\\right) \\times p_\\text{T}^{2}\\right)^{-n_\\text{pd}}\n",
    "$$\n",
    "\n",
    "In the following cell, we will produce a template for each simulated process by applying the same event selection as in data and creating a histogram of the corresponding  $p_\\text{T}$ spectra."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "96e536a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "vars = [ROOT.RooRealVar('mass', 'm', 2.4, 4.2, 'GeV/c^{2}'), ROOT.RooRealVar('arap', '|y|', 0.0, 2.4, ''), ROOT.RooRealVar('pt', 'p_{T}', 0.0, 3.0, 'GeV/c')]\n",
    "\n",
    "#extract the simulated data\n",
    "def getDataset(inname):\n",
    "    #extract rdataframe\n",
    "    ROOT.ROOT.EnableImplicitMT()\n",
    "    df = ROOT.RDataFrame('Events', inname)\n",
    "    \n",
    "    #apply selections\n",
    "    df = df.Filter('rec_dimuon_charge==0 && rec_dimuon_selection') \\\n",
    "           .Filter('rec_dimuon_softid && rec_dimuon_trigger && event_preselection')\n",
    "    \n",
    "    #convert dataframe\n",
    "    df = pd.DataFrame(df.AsNumpy(['rec_dimuon_pt', 'rec_dimuon_rapidity', 'rec_dimuon_mass']))\n",
    "    \n",
    "    #convert to RooDataSet\n",
    "    data = ROOT.RooDataSet.from_numpy({\"mass\": df['rec_dimuon_mass'], \"arap\": np.abs(df['rec_dimuon_rapidity']), \"pt\": df['rec_dimuon_pt']}, vars)\n",
    "    data.SetName('data')\n",
    "    \n",
    "    print(f'Processed: {len(df)} simulated events from {inname}')\n",
    "    ROOT.ROOT.DisableImplicitMT()\n",
    "    return data\n",
    "\n",
    "\n",
    "inputs = {'CohJpsiToMuMu': '/eos/user/c/cmsdas/2023/long-ex-hin/simulation/HiForestAOD_CohJpsiToMuMu.root',\n",
    "          'CohPsi2SFeeddownToMuMu': '/eos/user/c/cmsdas/2023/long-ex-hin/simulation/HiForestAOD_CohPsi2SFeeddownToMuMu.root',\n",
    "          'InCohJpsiToMuMu': '/eos/user/c/cmsdas/2023/long-ex-hin/simulation/HiForestAOD_InCohJpsiToMuMu.root',\n",
    "          'LowMassGammaGammaToMuMu': '/eos/user/c/cmsdas/2023/long-ex-hin/simulation/HiForestAOD_LowMassGammaGammaToMuMu.root'}\n",
    "\n",
    "dataset = {}\n",
    "for key, value in inputs.items():\n",
    "    dataset[key] = getDataset(f'skim/{key}.root')\n",
    "    \n",
    "#extract the real data\n",
    "df = ROOT.RDataFrame('Events','skim/data.root')\n",
    "df = df.Filter('rec_dimuon_charge==0 && rec_dimuon_softid')\n",
    "df = pd.DataFrame(df.AsNumpy(['rec_dimuon_mass', 'rec_dimuon_rapidity', 'rec_dimuon_pt']))\n",
    "dataset['data'] = ROOT.RooDataSet.from_numpy({\"mass\": df['rec_dimuon_mass'], \"arap\": np.abs(df['rec_dimuon_rapidity']), \"pt\": df['rec_dimuon_pt']}, vars)\n",
    "dataset['data'].SetName('data')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8cb2857",
   "metadata": {},
   "source": [
    "Next we procede to prepare the model in the workspace, by importing each template as a RooHistPdf."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6efc2668",
   "metadata": {},
   "outputs": [],
   "source": [
    "w = {}\n",
    "bins = [1.6, 1.9, 2.1, 2.4]\n",
    "\n",
    "for i in range(3):\n",
    "    print(f'Preparing workspace for bin: {bins[i]} <= |y| < {bins[i+1]} , 2.95 < mass < 3.25 GeV/c^2')\n",
    "    #start a fitting workspace\n",
    "    w[i] = ROOT.RooWorkspace(\"w\")\n",
    "\n",
    "    #define the bin variables\n",
    "    w[i].factory('arap[0.0, 2.5]')\n",
    "    w[i].factory('mass[2.6,4.0]')\n",
    "\n",
    "    #define the observable\n",
    "    w[i].factory('pt[1E-6, 3]')\n",
    "    w[i].var('pt').setBins(150)\n",
    "\n",
    "    #create model\n",
    "    ylist = 'N_InCohJpsiToMuMuWithDis[1E4, 0, 1E6] * model_InCohJpsiToMuMuWithDis'\n",
    "    w[i].factory(\"RooGenericPdf::model_InCohJpsiToMuMuWithDis('@0 * TMath::Power(1 + (@1 / @2)*@0*@0, -@2)', {pt, bpd[1.79, 0, 5], npd[3.58, 0, 10]})\")\n",
    "\n",
    "    #add templates\n",
    "    for key, ds in dataset.items():\n",
    "        if key=='data': continue\n",
    "        data = ds.reduce(w[i].var('pt'), f'(arap>={bins[i]} && arap<{bins[i+1]}) && (mass>2.95 && mass<3.25) && pt>=0')\n",
    "        data = data.binnedClone()\n",
    "        data.SetName(key)\n",
    "        w[i].Import(data)\n",
    "        pdf = ROOT.RooHistPdf(f'model_{key}', '', w[i].var('pt'), w[i].data(key), 0)\n",
    "        w[i].Import(pdf)\n",
    "        ylist += f', N_{key}[1E4, 0, 1E6] * model_{key}'\n",
    "\n",
    "    w[i].factory(f'SUM::model({ylist})')\n",
    "\n",
    "    #now import the data\n",
    "    data = dataset['data']\n",
    "    data = data.reduce(w[i].var('pt'), f'(arap>={bins[i]} && arap<{bins[i+1]}) && (mass>2.95 && mass<3.25) && pt>=0')\n",
    "    w[i].Import(data)\n",
    "    \n",
    "    #snapshot initial values\n",
    "    w[i].saveSnapshot('initial_setting', w[i].allVars())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d54c07b",
   "metadata": {},
   "source": [
    "Then we fit the results and compute the yields in the $p_\\text{T} < 0.2$ GeV kinematic region (same as used in the invariant mass fit)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1483f0b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import ROOT.RooFit as RF\n",
    "\n",
    "#perform fit\n",
    "for i in range(3):\n",
    "    print(f'Performing pt fit for bin: {bins[i]} <= |y| < {bins[i+1]} , 2.95 < mass < 3.25 GeV/c^2')\n",
    "    w[i].loadSnapshot('initial_setting')\n",
    "\n",
    "    data = w[i].data('data')\n",
    "    fitResult = w[i].pdf('model').fitTo(data, RF.NumCPU(2), RF.PrintLevel(-1), RF.Save(1))\n",
    "    fitResult.Print()\n",
    "    \n",
    "    #extract results at pt < 0.2 GeV/c\n",
    "    yields = {}\n",
    "    w[i].var('pt').setRange(\"signal\", 0, 0.2)\n",
    "    for key in ['CohJpsiToMuMu', 'InCohJpsiToMuMu', 'InCohJpsiToMuMuWithDis']:\n",
    "        yields[key] = w[i].pdf(f'model_{key}').createIntegral(w[i].var('pt'), RF.NormSet(w[i].var('pt')), RF.Range(\"signal\")).getVal()\n",
    "        yields[key] *= w[i].var(f'N_{key}').getVal()\n",
    "    \n",
    "    print(f'Results for bin: pt < 0.2 GeV/c , {bins[i]} <= |y| < {bins[i+1]} , 2.95 < mass < 3.25 GeV/c^2')\n",
    "    fI = (yields['InCohJpsiToMuMu'] + yields['InCohJpsiToMuMuWithDis'])/yields['CohJpsiToMuMu']\n",
    "    print(f'N_CohJpsiToMuMu = {yields[\"CohJpsiToMuMu\"]}')\n",
    "    print(f'f_I = {fI}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d786a29",
   "metadata": {},
   "source": [
    "Finally we display the fit results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "664291bb",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "canvas=[]\n",
    "components= ['model_CohJpsiToMuMu', 'model_CohPsi2SFeeddownToMuMu',\n",
    "             'model_InCohJpsiToMuMu', 'model_InCohJpsiToMuMuWithDis',\n",
    "             'model_LowMassGammaGammaToMuMu']\n",
    "for i in range(3):\n",
    "    canvas.append(\n",
    "        plotFitResults(w[i],\n",
    "                       model=w[i].pdf('model'),\n",
    "                       data=w[i].data('data'),\n",
    "                       name=f'w{i}',\n",
    "                       varname='pt',\n",
    "                       components=components,\n",
    "                       component_colors=[2,3,4,6,12])\n",
    "    )\n",
    "    canvas[i].cd(1).SetLogy()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b31fb5a2",
   "metadata": {},
   "source": [
    "Inspect the quality of the fits and see if they can be improved:\n",
    "* One way to improve the fits is by constraining the normalization of the templates from feed-down ($\\psi(\\text{2S}) \\rightarrow \\text{J}/\\psi + \\text{X} \\rightarrow \\mu^{+}\\mu^{-} + \\text{X}$) and by fixing the normalization of the QED background ($\\gamma\\gamma \\rightarrow \\mu^{+}\\mu^{-}$) to the results from the invariant mass fits.\n",
    "* Keep in mind the different invariant mass and $p_\\text{T}$ kinematic regions when comparing between different fits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3698dce8",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
