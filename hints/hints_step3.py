# Hint : compare 𝑝𝑇 before and after acceptance requirements
# we make use of the helpers.inspectDistribution method used in Step2

#define in the list of dataframes the inclusive and the dataframe after acceptance requirements
dflist=[(gendf,None,'Gen'),
        (gendf,gendf['gen_dimuon_acc']==True,'Gen+Acc'),
       ]

#call the plotting method
helpers.inspectDistribution(dflist,var='gen_dimuon_pt',xlabel=r'Di-muon $p_{T}$ [GeV]',
                            bins=np.logspace(-2,0.5,50),xscale='log')


#Hint : overlaying the different generated events

#create the list of dataframes (here takign all posible from mc_dict)
#notice we'll only load some of the columns to be plotted for simplicity
dflist=[ (pd.DataFrame( ROOT.RDataFrame('Events',f'skim/{k}.root') \
                            .AsNumpy(columns=['rec_dimuon_pt','rec_dimuon_mass']) ),
          None,
          k) 
          for k in mc_dict.keys() ]

#call the plotting method
helpers.inspectDistribution(dflist,var='rec_dimuon_pt',xlabel=r'Di-muon $p_{T}$ [GeV]',
                            bins=np.logspace(-2,0.5,50),xscale='log')