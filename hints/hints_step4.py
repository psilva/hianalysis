# Hint : compute the acceptance as function of |y| and print the result

#define as numerator and denominator the events in different rapidity bins after and before acceptance cuts
bins=np.array([0,1.6,1.9,2.1,2.4])
mask_pass=(gendf['gen_dimuon_acc']==True)
num = np.histogram(gendf[mask_pass]['gen_dimuon_absrapidity'],bins=bins)[0]
den = np.histogram(gendf['gen_dimuon_absrapidity'],bins=bins)[0]

#compute the efficiency
eff,eff_unc = helpers.computeEfficiency(num,den)

#show the efficiency plot
helpers.displayEfficiencyPlot(bins,eff,eff_unc,xlabel='Di-muon absolute rapidity')

for i in range(len(bins)-1):
    avg_unc=0.5*(eff[0][i]+eff[1][i])
    print(f'Acceptance for bin: {bins[i]} <= |y| < {bins[i+1]} = {eff[i]:3.4f} +/- {avg_unc:3.4f}')
    

# Hint : compute the efficency as function of |y| and print the result

#bins in |y|
absetabins=np.array([0,1.6,1.9,2.1,2.4])

#compute the efficiency
num = np.histogram(df[mask_pass]['gen_dimuon_absrapidity'],bins=absetabins)[0]
den = np.histogram(df['gen_dimuon_absrapidity'],bins=absetabins)[0]
eff,eff_unc = helpers.computeEfficiency(num,den)

#show the plot
helpers.displayEfficiencyPlot(absetabins,eff,eff_unc,xlabel='Di-muon absolute rapidity',ylabel='Efficiency')


# Hint: display the combined acceptance times efficiency

#multiply and propagate uncertainties
ae = acc*sfeff
ae_unc = np.hypot( acc*sfeff_unc, sfeff*acc_unc)

#show the plot
helpers.displayEfficiencyPlot(etabins,ae,ae_unc,xlabel='Di-muon rapidity',ylabel=r'A x SF x $\varepsilon$')