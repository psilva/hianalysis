{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a9715a21",
   "metadata": {},
   "source": [
    "#  Coherent J/$\\psi$ production in ultraperipheral PbPb collisions at $\\sqrt{s_{_{\\text{NN}}}}=5.02$ TeV\n",
    "\n",
    "## Classify event via neutron multiplicity\n",
    "\n",
    "This notebook is part of the Heavy Ion Exercise for the CERN CMSDAS School 2023.\n",
    "\n",
    "Because of high photon flux, the photon induced interactions ($\\gamma\\gamma$ and $\\gamma$A) have a high probability to be accompanied by additional photon exchanges that excite one or both of the ions into giant dipole resonances (GDRs) or higher excitations in UPCs. The GDRs typically decay by emitting a single neutron, while higher resonances usually decay by emitting two or more neutrons [[RMP 47 (1975) 713](http://dx.doi.org/10.1103/RevModPhys.47.713)]. These neutrons have low transverse momentum with respect to their parent ion, so largely retain the beam rapidity and are easily detected by ZDC (plus and minus) located in forward rapidity of CMS detector.\n",
    "\n",
    "To study the neutron multiplicity dependence, the events can be classified into 3 neutron multiplicity classes, which are labeled ”0n0n”, ”0nXn”, and ”XnXn” (X ≥ 1), where,\n",
    "* **0n0n**: No neutron emission from either nucleus.\n",
    "* **Xn0n**: While on neutron emitted from one nucleus, there are ≥1 neutrons are emitted from the other one.\n",
    "* **XnXn**: Emitted neutrons should be ≥ 1 from each nucleus.\n",
    "\n",
    "More interestingly, the contribution of higher excitations becomes larger as impact parameter becomes smaller [[PR 458 (2008) 878](http://dx.doi.org/10.1016/j.physrep.2007.12.001)]. Therefore, the forward neutron multiplicity can be used to classify UPC events into different impact parameter ranges, while high neutron multiplicity corresponds to small mean impact parameter and low neutron multiplicity corresponds to large mean impact parameter ($\\langle\\text{b}_\\text{XnXn}\\rangle < \\langle\\text{b}_\\text{Xn0n}\\rangle < \\langle\\text{b}_\\text{0n0n}\\rangle$), as shown in Figure 1.\n",
    "\n",
    "![Neutron multiplicity](https://www.annualreviews.org/na101/home/literatum/publisher/ar/journals/content/nucl/2020/nucl.2020.70.issue-1/annurev-nucl-030320-033923/20201016/images/large/nucl700323.f2.jpeg)\n",
    "\n",
    "_Figure 1_ - (a) Impact parameter dependence of the probabilities, from STARLight, of the three primary forward neutron topologies: 0n0n, or no neutron emission in either direction, which selects impact parameters $\\text{b} > 40$ fm; Xn0n, with neutron emission in only one direction, which selects impact parameters of $\\text{b} \\approx 20$ fm; and XnXn, with neutron emission in both directions, which selects impact parameters $\\text{b} < 15$ fm. (b) STARLight calculation of the impact parameter dependence of coherent $\\rho$ production, assuming different ZDC fragmentation scenarios. The difference between mutual Coulomb excitation and no breakup is quite stark at larger impact parameters. Figure sourced from [ARNPS 70 (2020) 323](https://www.annualreviews.org/doi/10.1146/annurev-nucl-030320-033923)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fba6a6ea",
   "metadata": {},
   "source": [
    "## Determine neutron multiplicity based on the ZDC distributions\n",
    "\n",
    "After the collision takes place, charge particles are deflected at the beam crossing point and only neutral particles (e.g. neutrons and photons) at $\\left|\\eta\\right|>8.3$ reach the ZDC located at 140 m, as shown in Figure 2.\n",
    "\n",
    "![ZDC](img/zdc.png)\n",
    "_Figure 2_ - Plan view of the section between the interaction point (IP) and the ZDC. Due to the beam optics, charged particles are deflected while the neutral particles travel directly to the ZDCs (one at each side of the IP). Figure from [ATL-FWD-PROC-2013-001](https://cds.cern.ch/record/1628749/files/ATL-FWD-PROC-2013-001.pdf).\n",
    "\n",
    "The ZDC has been designed with an electromagnetic and hadronic section that separates photons and neutrons, and detect high energy neutrons with ~100% efficiency. Therefore, forward neutron multiplicities can be derived by analyzing the energy distributions (recorded as Analog-Digital Converter (ADC) units) of these neutrons deposited in ZDC.\n",
    "\n",
    "![ZDC](https://inspirehep.net/files/0983b0d673d1c5be43a92c3dff7c049e)\n",
    "\n",
    "_Figure 3_ - Schematic side-view of the CMS ZDC. The calorimeter is placed at an angle $\\theta<0.5$ mrad, corresponding to $\\left|\\eta\\right|>8.3$ inside a special detector slot in the neutral particle absorber (TAN), which is used to protect the first LHC superconducting quadrupole magnet from radiation. The sampling sections interleave W and quartz fibers with 5 horizontal electromagnetic divisions and 4 longitudinal hadronic divisions. The electromagnetic (hadronic) section totals 19 $X_0$ (5.6$\\lambda$). Figure sourced from [JINST 16 (2021) 05, P05008](https://arxiv.org/pdf/2102.06640.pdf).\n",
    "\n",
    "\n",
    "In the following cell we will open the skim data and display the minus vs. plus ZDC ADC distributions from events containing at least one $\\mu^{-}\\mu^{+}$ pair."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81d5f08e",
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "import ROOT\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import sys\n",
    "sys.path.append('snippets')\n",
    "import helpers\n",
    "from fit_plot import *\n",
    "ROOT.gStyle.SetOptStat(0)\n",
    "ROOT.gStyle.SetOptTitle(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8905071",
   "metadata": {},
   "outputs": [],
   "source": [
    "#extract the ZDC ADC from data\n",
    "ROOT.ROOT.EnableImplicitMT()\n",
    "\n",
    "#pre-selected data (filled bunches)\n",
    "df = ROOT.RDataFrame('ZDC','skim/data_zdc.root')\n",
    "df = pd.DataFrame(df.AsNumpy(['zdcPlus', 'zdcMinus']))\n",
    "\n",
    "# empty bunch data\n",
    "emptydf = ROOT.RDataFrame('ZDC','skim_emptybx/data_emptybx.root')\n",
    "emptydf = pd.DataFrame(emptydf.AsNumpy(['zdcPlus', 'zdcMinus']))\n",
    "\n",
    "ROOT.ROOT.DisableImplicitMT()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dbd3179f",
   "metadata": {},
   "source": [
    "Make some control distributions of the energy deposits in the ZDCs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5f816bb6",
   "metadata": {},
   "outputs": [],
   "source": [
    "#inpect the 1D distributions of energy deposits in the ZDC\n",
    "dflist=[(df,None,'Filled BX'),(emptydf,None,'Empty BX')]\n",
    "\n",
    "helpers.inspectDistribution(dflist,var='zdcPlus',xlabel=r'ZDC+ (a.u.)',\n",
    "                            bins=np.linspace(0, 35000, 100+1),xscale='linear',yscale='log')\n",
    "helpers.inspectDistribution(dflist,var='zdcMinus',xlabel=r'ZDC- (a.u.)',\n",
    "                            bins=np.linspace(0, 35000, 100+1),xscale='linear',yscale='log')\n",
    "\n",
    "#2D correlations\n",
    "helpers.showCorrelation(df,x='zdcPlus',y='zdcMinus',\n",
    "                        xbins=np.linspace(0, 35000, 100),\n",
    "                        ybins=np.linspace(0, 35000, 100),\n",
    "                        xlabel='ZDC+',ylabel='ZDC-')\n",
    "\n",
    "helpers.showCorrelation(emptydf,x='zdcPlus',y='zdcMinus',\n",
    "                        xbins=np.linspace(0, 35000, 100),\n",
    "                        ybins=np.linspace(0, 35000, 100),\n",
    "                        xlabel='ZDC+',ylabel='ZDC-')\n",
    "\n",
    "# prevent (scrollable) sub-window; change 140em if you need to extend further\n",
    "# https://stackoverflow.com/questions/18770504/resize-ipython-notebook-output-window\n",
    "from IPython.core.display import display, HTML\n",
    "display(HTML(\"<style>div.output_scroll { height: 140em; }</style>\"))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09f3d28b",
   "metadata": {},
   "source": [
    "##  Neutron class purity\n",
    "\n",
    "One needs to ensure that the contamination in each neutro category is minimized. In the case of 0 neutrons, there is no activity in the ZDC, however there is always the intrinsic noise present in the detector. The ZDC noise distributions are studied by empty bunch crossing events triggered by HLT_HIL1NotBptxOR and stored in the /HIEmptyBX/HIRun2018A-27Feb2019-v1/AOD dataset.\n",
    "\n",
    "The ZDC ADC thresholds for 0n and Xn (X > 1) are defined such that the noise contamination is kept <0.02% for either , and shown in the table below:\n",
    "\n",
    "\n",
    "|Neutron multiplicity| ZDC ADC range      |\n",
    "|------------------- | -------------------|\n",
    "| 0n (Plus)          | ADC < 4200         |\n",
    "| Xn (X≥1, Plus)     | 4200 < ADC < 50000 |\n",
    "| 0n (Minus)         | ADC < 6000         |\n",
    "| Xn (X≥1, Minus)    | 6000 < ADC < 60000 |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0dc51a82",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Requirements for empty ZDC <0.02% noise (compare to final values in the table above)')\n",
    "print(f\"0n+ => ADC<{emptydf['zdcPlus'].quantile(0.9998)}\")\n",
    "print(f\"0n- => ADC<{emptydf['zdcMinus'].quantile(0.9998)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb47766d",
   "metadata": {},
   "source": [
    "Next we will proced to determine the efficiency of the ZDC ADC cut on the Xn (X > 1) multiplicity class. To accomplish this we will fit the plus and minus ZDC ADC distributions using multi-Gaussian functions.\n",
    "\n",
    "Let's start by defining 3 Gaussian PDFs to fit the 1n, 2n and 3n neutron multiplicity peaks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "96e536a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "#prepare workspace for fitting ZDC energy\n",
    "workspace = {}\n",
    "\n",
    "for var in ['zdcPlus', 'zdcMinus']:\n",
    "    print(f'Preparing workspace for {var}')\n",
    "    #start a fitting workspace\n",
    "    w = ROOT.RooWorkspace(\"w\")\n",
    "\n",
    "    #define the observable\n",
    "    vmax = 3.5E4 if var==\"zdcPlus\" else 5E4\n",
    "    w.factory(f'{var}[0, {vmax}]')\n",
    "    w.var(var).setBins(200)\n",
    "    w.var(var).setRange('full', 0, vmax)\n",
    "\n",
    "    #create model\n",
    "    w.factory(\"RooFormulaVar::sigma3('@0*@1', {sf3[1.3, 0.9, 1.4], sigma2[3.2E3, 1, 7E3]})\")\n",
    "    w.factory(f\"RooGaussian::model_G1({var}, mu1[8E3, 0, 20E3], sigma1[1.6E3, 1, 5E3])\")\n",
    "    w.factory(f\"RooGaussian::model_G2({var}, mu2[14E3, 8E3, 30E3], sigma2)\")\n",
    "    w.factory(f\"RooGaussian::model_G3({var}, mu3[23E3, 14E3, 50E3], sigma3)\")\n",
    "    w.factory(\"SUM::model(N_G1[1E4, 0, 1E6] * model_G1, N_G2[1E4, 0, 1E6] * model_G2, N_G3[1E4, 0, 1E6] * model_G3)\")\n",
    "    w.saveSnapshot('initial_setting', w.allVars())\n",
    "\n",
    "    #now import the data\n",
    "    data = ROOT.RooDataSet.from_numpy({var: df[var]}, [w.var(var)])\n",
    "    data.SetName('data')\n",
    "    w.Import(data)\n",
    "    \n",
    "    workspace[var] = w"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "174911ef",
   "metadata": {},
   "source": [
    "Then we proceed to fit the plus and minus ZDC ADC distributions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d798b3dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "import ROOT.RooFit as RF\n",
    "\n",
    "#perform fit\n",
    "for var, w in workspace.items():\n",
    "    print(f'Fitting {var}')\n",
    "    w.loadSnapshot('initial_setting')\n",
    "    if var==\"zdcPlus\":\n",
    "        w.var(var).setRange('fit', 4E3, 30E3)\n",
    "    else:\n",
    "        w.var(var).setRange('fit', 5E3, 40E3)\n",
    "    w.pdf('model').fitTo(w.data('data'), RF.NumCPU(2), RF.PrintLevel(-1), RF.Save(1), RF.Range('fit')).Print()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39e1d961",
   "metadata": {},
   "source": [
    "Next we proceed to display fit results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6f8fd97c",
   "metadata": {},
   "outputs": [],
   "source": [
    "canvas=[]\n",
    "for var,w in workspace.items():\n",
    "    canvas.append(\n",
    "        plotFitResults(w,\n",
    "                       model=w.pdf('model'),\n",
    "                       data=w.data('data'),\n",
    "                       varname=var,\n",
    "                       vartitle=var,\n",
    "                       components=[f'model_G{i+1}' for i in range(3)],\n",
    "                       component_colors=[2,3,4,6,12],\n",
    "                       ran='fit')\n",
    "    )\n",
    "    canvas[-1].cd(1).SetLogy()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a940909",
   "metadata": {},
   "source": [
    "Now that we have determined the ZDC ADC thresholds and the corresponding neutron multiplicity peak distributions, we can:\n",
    "\n",
    "* For those feeling **bold** : \n",
    "  * Include the noise template derived from the empty bunches in the fit and estimate the purity and efficiency using all the ingredients\n",
    "  * Using the result of the fit and the table of cuts on the ZDC energy derive the confusion matrix \n",
    "* In groups: go back to the starting point and redo the J/$\\psi$ signal extraction after applying the neutron multiplicity classes: 0n0n, Xn0n and XnXn."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d6016d1",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
