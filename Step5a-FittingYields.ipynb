{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a9715a21",
   "metadata": {},
   "source": [
    "#  Coherent J/$\\psi$ production in ultraperipheral PbPb collisions at $\\sqrt{s_{_{\\text{NN}}}}=5.02$ TeV\n",
    "\n",
    "## Fitting signal yields and muon efficiencies\n",
    "\n",
    "This notebook is part of the Heavy Ion Exercise for the CERN CMSDAS School 2023.\n",
    "\n",
    "We assume that you have skimmed the data, made a first inspection and you are now ready fit the yields of the signal."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3173e7f",
   "metadata": {},
   "source": [
    "## Fitting signal yields\n",
    "\n",
    "Let's start by fitting the J$/\\psi$ mass peak to extract the signal yields in different phase space regions.\n",
    "\n",
    "To this aim we'll make use of [RooFit](https://root.cern/manual/roofit/)\n",
    "\n",
    "The total number of events be written as a sum of real J$/\\psi$ ($N_\\text{S}$) with background contributions ($N_\\text{B}$).\n",
    "\n",
    "Model the signal contribution with a [Crystal-Ball PDF](https://en.wikipedia.org/wiki/Crystal_Ball_function) while the background will be modelled based on a set of [Chebyshev polynomials](https://en.wikipedia.org/wiki/Chebyshev_polynomials)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c50fa67",
   "metadata": {},
   "outputs": [],
   "source": [
    "import ROOT\n",
    "import ROOT.RooFit as RF\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import sys\n",
    "sys.path.append('snippets')\n",
    "import helpers\n",
    "ROOT.EnableImplicitMT()\n",
    "ROOT.gStyle.SetOptStat(0)\n",
    "ROOT.gStyle.SetOptTitle(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51f18cdd",
   "metadata": {},
   "source": [
    "### Data preparation and inspection\n",
    "\n",
    "Data needs to be prepared before the fit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e91290e8",
   "metadata": {},
   "outputs": [],
   "source": [
    "#select further the events from the skimmed data\n",
    "#require one of the muons to pass the soft muon selection criteria and an op. sign dilepton pair\n",
    "#define the kinematics of the probe muon\n",
    "rdf=ROOT.RDataFrame('Events','skim/data.root')\n",
    "rdf = rdf.Filter('rec_dimuon_charge==0 && rec_dimuon_softid') \\\n",
    "         .Define('rec_muon1_pass', 'rec_muon1_softid && rec_muon1_trigger') \\\n",
    "         .Define('rec_muon2_pass', 'rec_muon2_softid && rec_muon2_trigger') \\\n",
    "         .Filter('rec_muon1_pass || rec_muon2_pass') \\\n",
    "         .Define('rec_muon_probe_pt','rec_muon1_pass>0 ? rec_muon2_pt : rec_muon1_pt') \\\n",
    "         .Define('rec_muon_probe_eta','rec_muon1_pass>0 ? rec_muon2_eta : rec_muon1_eta') \\\n",
    "         .Define('rec_muon_probe_pass','rec_muon1_pass>0 ? rec_muon2_pass : rec_muon1_pass') \n",
    "\n",
    "df = pd.DataFrame(rdf.AsNumpy(['rec_dimuon_mass','rec_dimuon_pt','rec_dimuon_rapidity','rec_muon_probe_pt','rec_muon_probe_eta','rec_muon_probe_pass']))\n",
    "mask_pass=(df['rec_muon_probe_pass']==1)\n",
    "df['rec_dimuon_absrapidity']=np.abs(df['rec_dimuon_rapidity'])\n",
    "print(f'Selected {mask_pass.sum()} ({(~mask_pass).sum()}) events in SR (CR)')\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3b57204",
   "metadata": {},
   "source": [
    "Do some basic visual inspection that it makes sense"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c937a515",
   "metadata": {},
   "outputs": [],
   "source": [
    "#define in the list of dataframes the inclusive and the dataframe after acceptance requirements\n",
    "dflist=[(df,mask_pass,'pass'), (df,~mask_pass,'fail')]\n",
    "\n",
    "#call the plotting method\n",
    "helpers.inspectDistribution(dflist,var='rec_dimuon_mass',xlabel=r'Di-muon mass [GeV]',\n",
    "                            bins=np.linspace(2.7,4.2,75),xscale='linear')\n",
    "helpers.inspectDistribution(dflist,var='rec_dimuon_pt',xlabel=r'Di-muon $p_T$ [GeV]',\n",
    "                            bins=np.logspace(-2,0.5,50),xscale='log')\n",
    "helpers.inspectDistribution(dflist,var='rec_dimuon_rapidity',xlabel=r'Di-muon rapidity',\n",
    "                            bins=np.linspace(1,3,50),xscale='linear')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7eaa6a37",
   "metadata": {},
   "source": [
    "### Create the fit model\n",
    "\n",
    "The construction of observable, the PDF and the datasets to be fit is easily done using a RooFit workspace which contains all the relevant parameters and data, and using the declarative instructions of the workspace factory.\n",
    "First time around the instructions may be cryptic but they'll prove to be powerful."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc06665c",
   "metadata": {},
   "outputs": [],
   "source": [
    "w = ROOT.RooWorkspace(\"w\") #start a fitting workspace\n",
    "\n",
    "#define the observable\n",
    "w.factory('mass[2.5,4.1]')\n",
    "w.factory('absy[1.9,2.4]')\n",
    "\n",
    "#constrain Psi(2S) parameters to J/Psi\n",
    "w.factory(\"RooFormulaVar::N_Psi2S('@0*@1', {R[0.2, 0, 2], N_JPsi[1E4, 0, 1E6]})\")\n",
    "w.factory(f\"RooFormulaVar::mu_Psi2S('@0*@1', {{mu[3.1, 3.0, 3.2], m_Psi2S_over_JPsi[{3.686097/3.0969}]\\}})\")\n",
    "w.factory(\"RooFormulaVar::sigma_Psi2S('@0*@1', {sigma[0.05, 0.01, 0.10], m_Psi2S_over_JPsi})\")\n",
    "\n",
    "#model in the signal region : Crystal-Ball for signal + polynominal for background\n",
    "#notice that the parameters of the PDFs are declared on the fly with their own ranges\n",
    "w.factory(\"CBShape::model_JPsi(mass, mu, sigma, alpha[1.8, -10.0, 10.0], n[5.0, 0.0, 30.0])\")\n",
    "w.factory(\"CBShape::model_Psi2S(mass, mu_Psi2S, sigma_Psi2S, alpha, n)\")\n",
    "w.factory(\"Chebychev::model_QED(mass, {p1[0, -1, 1], p2[0, -1, 1], p3[0, -1, 1]})\")\n",
    "w.factory(\"SUM::model(N_JPsi * model_JPsi, N_Psi2S * model_Psi2S, N_QED[1E4, 0, 1E6] * model_QED)\")\n",
    "\n",
    "#now import the data within kinematic bin\n",
    "df['rec_dimuon_absrapidity'] = np.abs(df['rec_dimuon_rapidity'])\n",
    "mask_bin = (df['rec_dimuon_pt']<0.2) & (df['rec_dimuon_absrapidity']>=1.9) & (df['rec_dimuon_absrapidity']<2.4)\n",
    "data = ROOT.RooDataSet.from_numpy(\n",
    "    {\"mass\": df[mask_pass & mask_bin]['rec_dimuon_mass'],\n",
    "     \"absy\": df[mask_pass & mask_bin]['rec_dimuon_absrapidity']}, \n",
    "    [w.var('mass'),w.var('absy')]\n",
    ")\n",
    "data.SetName('data')\n",
    "w.Import(data)\n",
    "\n",
    "#import also the data in the failed control region\n",
    "data_fail = ROOT.RooDataSet.from_numpy(\n",
    "    {\"mass\": df[~mask_pass & mask_bin]['rec_dimuon_mass'],\n",
    "     \"absy\": df[~mask_pass & mask_bin]['rec_dimuon_absrapidity']}, \n",
    "    [w.var('mass'),w.var('absy')])\n",
    "data_fail.SetName('data_fail')\n",
    "w.Import(data_fail)\n",
    "\n",
    "#print the contents of the workspace\n",
    "w.Print('v')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26a78ac9",
   "metadata": {},
   "source": [
    "Running the fit is a simple operation, as shown below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "718ccfd5",
   "metadata": {},
   "outputs": [],
   "source": [
    "#perform fit\n",
    "fitResult = w.pdf('model').fitTo(data, RF.NumCPU(2), RF.PrintLevel(-1), RF.Save(1))\n",
    "fitResult.Print()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "052ada19",
   "metadata": {},
   "source": [
    "Now we inspect visually the result of the fit.\n",
    "\n",
    "Note: the function below is a bit more generic for reasons that will become clearer at the end of the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd0ca3a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%load -s plotFitResults snippets/fit_plot.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "66ac620a",
   "metadata": {},
   "outputs": [],
   "source": [
    "c=plotFitResults(w,model=w.pdf('model'),data=data)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "269f0fc0",
   "metadata": {},
   "source": [
    "You can inspect the fit results by looping over the parameters of the model.\n",
    "\n",
    "It is is also useful to save a snapshot of the fit results.\n",
    "\n",
    "We do both in the following cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d3be4a7",
   "metadata": {},
   "outputs": [],
   "source": [
    "params=w.pdf('model').getParameters(w.var('mass'))\n",
    "for p in params:\n",
    "    print(f\"{p.GetName()} = {p.getVal():.3f} +/- {p.getError():.3f}\")\n",
    "    \n",
    "w.saveSnapshot(\"inclusive_fit\", params, True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb10c338",
   "metadata": {},
   "source": [
    "As we have concluded, we store the final workspace in a ROOT file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "59ce7334",
   "metadata": {},
   "outputs": [],
   "source": [
    "#save the workspace to a file\n",
    "w.writeToFile('jpsiyields.root',True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73217874",
   "metadata": {},
   "source": [
    "## Next steps\n",
    "\n",
    "Let's use the workspace saved in the next notebook to expand a bit further the model so that it extracts also the scale factor. Please move to `Step5b-FittingYieldsAndEfficiency.ipynb`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67ed541f",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
