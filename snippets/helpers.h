#ifndef hianalysis_helpers_h
#define hianalysis_helpers_h

#include <cmath>
#include "ROOT/RVec.hxx"
#include "TMath.h"
#include "TVector2.h"
#include "tnp_muon_UPC_PbPb2018.h"


using rvec_f = const ROOT::VecOps::RVec<float>;
using rvec_i = const ROOT::VecOps::RVec<int>;
using rvec_b = const ROOT::VecOps::RVec<bool>;
const float mu_mass(0.1056583755);


/**
    @short wrapper to build a vector of id flags for muons
*/

// Get dimuon mask based on single muon mask
rvec_i dimuon_mask(const rvec_i &imu1, const rvec_i &imu2, const rvec_i &muon_mask)
{
    std::vector<int> mask(imu1.size(), 0);
    for(size_t i=0; i<imu1.size(); i++)
        mask[i] = muon_mask[imu1[i]]>0 && muon_mask[imu2[i]]>0;
    return rvec_i(mask.begin(), mask.end());
}

// Match reconstructed muons to generated muons
rvec_i rec_muon_match_gen(const rvec_i &rec_charge, const rvec_f &rec_pt, const rvec_f &rec_eta, const rvec_f &rec_phi,
                          const rvec_i &gen_charge, const rvec_f &gen_pt, const rvec_f &gen_eta, const rvec_f &gen_phi)
{
    std::vector<int> gen_index(rec_pt.size(), -1);
    for(size_t i=0; i<gen_pt.size(); i++) {
        int index(-1);
        double min_deltaR(0.05);
        for(size_t j=0; j<rec_pt.size(); j++) {
            // match by charge
            if (rec_charge[j] != gen_charge[i])
                continue;
            // match by relative delta pt
            if (abs(rec_pt[j] - gen_pt[i])/gen_pt[i] >= 0.5)
                continue;
            // match by delta R
            auto deltaPhi = rec_phi[j] - gen_phi[i];
            deltaPhi += (deltaPhi > M_PI) ? -2.*M_PI : ((deltaPhi <= -M_PI) ? 2.*M_PI : 0);
            const auto deltaEta = rec_eta[j] - gen_eta[i];
            const auto deltaR = sqrt(deltaEta*deltaEta + deltaPhi*deltaPhi);
            // select pair that minimizes delta R
            if (deltaR < min_deltaR) {
                index = j;
                min_deltaR = deltaR;
            }
        }
        if (index >= 0)
            gen_index[index] = i;
    }
    return rvec_i(gen_index.begin(), gen_index.end());
}

// Acceptance for soft ID muons
rvec_b muon_acceptance_softid(const rvec_f &pt, const rvec_f &eta)
{
    std::vector<bool> accept(pt.size());
    for(size_t i=0; i<pt.size(); i++) {
        const auto abseta = std::abs(eta[i]);
        
        float minpt(1.E6);
        if (abseta>=0.0 && abseta<1.0)
            minpt = 3.3;
        else if (abseta>=1.0 && abseta<1.3)
            minpt = -4.00*abseta + 7.30;
        else if (abseta>=1.3 && abseta<1.7)
            minpt = -1.32*abseta + 3.25;
        else if (abseta>=1.7 && abseta<2.4)
            minpt = 1.0;
                             
        accept[i] = (pt[i] >= minpt);
    }
    return rvec_b(accept.begin(), accept.end());
}

// Acceptance for triggered muons
rvec_b muon_acceptance_trigger(const rvec_f &pt, const rvec_f &eta)
{
    std::vector<bool> accept(pt.size());
    for(size_t i=0; i<pt.size(); i++) {
        const auto abseta = std::abs(eta[i]);
        
        float minpt(1.E6);
        if (abseta>=0.0 && abseta<0.3)
            minpt = 3.45;
        else if (abseta>=0.3 && abseta<1.1)
            minpt = 3.3;
        else if (abseta>=1.1 && abseta<1.45)
            minpt = -3.28*abseta + 6.91;
        else if (abseta>=1.45 && abseta<1.65)
            minpt = 2.15;
        else if (abseta>=1.65 && abseta<2.1)
            minpt = -2.11*abseta + 5.64;
        else if (abseta>=2.1 && abseta<2.4)
            minpt = 1.2;
                             
        accept[i] = pt[i] >= minpt;
    }
    return rvec_b(accept.begin(), accept.end());
}

// Tag-and-probe weights
rvec_f tnp_weight_softid(const rvec_f &pt, const rvec_f &eta, const int &idx=0)
{
    std::vector<float> weight;
    for(size_t i=0; i<pt.size(); i++)
        weight.push_back(tnp_weight_softid_upc_pbpb(pt[i], eta[i], idx));

    return rvec_f(weight.begin(), weight.end());
}
rvec_f tnp_weight_trigger(const rvec_f &pt, const rvec_f &eta, const int &idx=0)
{
    std::vector<float> weight;
    for(size_t i=0; i<pt.size(); i++)
        weight.push_back(tnp_weight_trigger_upc_pbpb(pt[i], eta[i], idx));

    return rvec_f(weight.begin(), weight.end());
}
float tnp_dimuon_weight_trigger(const rvec_f &w, const rvec_b &pass)
{
    if (w.size()<2)
        return 1;
    return (pass[0] ? w[0] : w[1]);
}

/**
    @short a wrapper for the 2-body system invariant mass
*/
float dimuon_mass(const rvec_f &pt, const rvec_f &eta, const rvec_f &phi)
{
    if (pt.size()<2)
        return -1;
    ROOT::Math::PtEtaPhiMVector p1(pt[0], eta[0], phi[0], mu_mass);
    ROOT::Math::PtEtaPhiMVector p2(pt[1], eta[1], phi[1], mu_mass);
    return (p1 + p2).M();
}

float dimuon_rapidity(const rvec_f &pt, const rvec_f &eta, const rvec_f &phi)
{
    if (pt.size()<2)
        return -99;
    ROOT::Math::PtEtaPhiMVector p1(pt[0], eta[0], phi[0], mu_mass);
    ROOT::Math::PtEtaPhiMVector p2(pt[1], eta[1], phi[1], mu_mass);
    return (p1 + p2).Rapidity();
}

float dimuon_pt(const rvec_f &pt, const rvec_f &phi)
{
    if (pt.size()<2)
        return -1;
    ROOT::Math::Polar2DVector p1(pt[0], phi[0]);
    ROOT::Math::Polar2DVector p2(pt[1], phi[1]);
    return (p1 + p2).R();
}

float dimuon_phi(const rvec_f &pt, const rvec_f &phi)
{
    if (pt.size()<2)
        return -1;
    ROOT::Math::Polar2DVector p1(pt[0], phi[0]);
    ROOT::Math::Polar2DVector p2(pt[1], phi[1]);
    return (p1 + p2).Phi();
}

#endif
